const fs=require("fs");
let input="./data.json";

readInput(input,(error,data)=>
{
    if(error)
    {
        console.error("Error occured while reading data.json"+error);
        return;
    }else{
        console.log("successfully readed input")
        retrieveData(data,(error,data) =>
        {
            if(error)
            {
                console.error("Error occured in retrieve data function"+error)
                return;
            }else{
                console.log("answerRetrieveData.json created");
                groupData(data,(error,data) =>
                {
                    if(error)
                    {
                        console.error("Error occured in groupData function"+error);
                        return;
                    }else
                    {
                        console.log("answerGroupData.json created");
                        powerPuffData(data,((error,data)=>
                        {
                            if(error)
                            {
                                console.error("Error occured in powerPuff Function"+error)
                                return;
                            }else
                            {
                                console.log("answerPowerPuffData.json created")
                                removeId(data,((error,data) =>
                                {
                                    if(error)
                                    {
                                        console.error("Error occured in removedId function"+error)
                                        return;
                                    }else{
                                        console.log("answerRemoveId.json created");
                                            sortBasedOnCompany(data,(error,data) =>
                                            {
                                                if(error)
                                                {
                                                    console.error("Error occured in sortBasedOnCompany function"+error)
                                                    return;
                                                }else{
                                                    console.log("answerSortBasedOnCampany.json created")
                                                    swap(data,(error,data) =>
                                                    {
                                                        if(error)
                                                        {
                                                            console.error("Error occured in swap function"+error)
                                                            return;
                                                        }else{
                                                            console.log("answerSwap.json created")
                                                            employeeBirthday(data,((error,data) =>
                                                            {
                                                                if(error)
                                                                {
                                                                    console.error("Error occured in emeployeeBirthday function"+error)
                                                                }else{
                                                                    console.log("answerEmployeeBirthday.json created")
                                                                }
                                                            }))
                                                        }
                                                    })
                                                }
                                            })
                                    }
                                }))
                            }
                        }) )
                    }
                })
            }
        })
    }
})



function readInput (input,callBack)
{
        fs.readFile("./data.json","utf-8",(error,data) =>
        {
            if(error)
            {
                callBack(err,null);
                return;
            }else 
            {
                callBack(null,data);
            }
        })
}

function retrieveData(inputData,callBack)
{
        let ids=[2,13,23];
        try{
        inputData=JSON.parse(inputData);
        }catch(error)
        {
            console.error(error)
        }
        let answer=inputData["employees"].filter((element) => ids.includes(element.id))
        try{
            answer=JSON.stringify(answer);
        }catch(error)
        {
            console.error(error);
        }
        fs.writeFile("./answer/answerRetrieveData.json",answer,"utf-8",(error) =>
        {
            if(error)
            {
                callBack(error,null)
                return;
            }else{
                callBack(null,inputData);
            }
        })
        
}



function groupData(inputData,callBack)
{

        let answer=inputData["employees"].reduce((accumulator,element) => 
        {
            if(accumulator[element.company])
            {
                accumulator[element["company"]].push(element)
            }
            return accumulator;

        },{ "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []})

    try{
        answer=JSON.stringify(answer);
    }catch(error)
    {
        console.log(error);
    }

        fs.writeFile("./answer/answerGroupData.json",answer,"utf-8",(error) =>
        {
            if(error)
            {
                callBack(error,null)
                return;
            }else{
                callBack(null,inputData);
              
            }
        })
    }



function powerPuffData(inputData,callBack)
{
            
        let answer=inputData["employees"].filter((element) => "Powerpuff Brigade".includes(element.company))
    
        try{
            answer=JSON.stringify(answer);
        }catch(error)
        {
            console.log(error);
        }
    
        fs.writeFile("./answer/answerPowerPuffData.json",answer,"utf-8",(error) =>
        {
            if(error)
            {
                callBack(error,null)
            }else 
            {
                callBack(null,inputData);
            }
        })
    }


function removeId(inputData,callBack)
{
      
    
        let answer=inputData["employees"].filter((element) =>(element.id !== 2))

        try{
            answer=JSON.stringify(answer);
        }catch(error)
        {
            console.log(error);
        }
    
        fs.writeFile("./answer/answerRemoveId.json",answer,"utf-8",(error) =>
        {
            if(error)
            {
                callBack(error,null)
            }else{
                callBack(null,inputData);
            }
        })
}


function sortBasedOnCompany(inputData,callBack)
{
        let answer=inputData.employees.sort((item1,item2)=>{
            if(item1.company<item2.company){
                return -1;
            }
            else if(item1.company>item2.company){
                return 1;
            }
            else{
                if(item1.id<item2.id){
                    return -1;
                }
                else if(item1.id>item2.id){
                    return 1;
                }
                else{
                    return 0;
                }
            }
            })
            try{
                answer=JSON.stringify(answer);
            }catch(error)
            {
                console.log(error);
            }

        fs.writeFile("./answer/answerSortBasedOnCampany.json",answer,"utf-8",(error) =>
        {
            if(error)
            {
                callBack(error,null);
            }else{
                callBack(null,inputData)
            }
        })
    }


function swap(inputData,callBack)
{
            let ninetyTwo=inputData.employees.filter((item)=>{
                return item.id===92;
            })
            let ninetyThree=inputData.employees.filter((item)=>{
                return item.id===93;
            })
            let answer=inputData.employees.map((item)=>{
                if(item.id===92){
                    return ninetyThree;
                }
                else if(item.id===93){
                    return ninetyTwo;
                }
                return item;
            })
            try{
                answer=JSON.stringify(answer);
            }catch(error)
            {
                console.log(error);
            }
        
        fs.writeFile("./answer/answerSwap.json",answer,"utf-8",(error) =>
        {
            if(error)
            {
                callBack(error,null)
            }else{
                callBack(null,inputData);
            }
        })
        }

function employeeBirthday(inputData,callBack)
{
    
            let answer=inputData.employees.map((item)=>{
                if((item.id)%2==0){
                    item["birthday"]=new Date().getDate();
                }
                return item;
            })
            
            try{
                answer=JSON.stringify(answer);
            }catch(error)
            {
                console.log(error);
            }
        
    
        fs.writeFile("./answer/answerEmployeeBirthday.json",answer,"utf-8",(error) =>
        {
            if(error)
            {
                callBack(error,null)
            }else{
                callBack(null,inputData)
            }
        })
        }









